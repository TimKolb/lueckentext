/** @jsx jsx */
// eslint-disable-next-line no-unused-vars
import React, { useEffect, useState } from "react";
import { ErrorMessage, Field, Form, Formik } from "formik";
import styled from "@emotion/styled";
import { jsx, css } from "@emotion/core";
import * as Yup from "yup";
import Snow from "react-snow-effect";

const fields = {
  promName: "Prominente Person",
  funnyAnimal: "Lustiges Tier",
  tld: "Top Level Domain",
  employee: "Incloud Mitarbeiter/in",
  funnyWord: "Lustiges Wort",
  language: "Sprache",
  petName: "Haustiername",
  badWord: "Negatives Adjektiv",
  goodWord: "Positives Adjektiv",
  subjectPlural: "Substantiv plural",
  verb: "Verb",
  timeInFuture: "Zeitpunkt in der Zukunft",
  subject: "Substantiv",
  timeFrame: "Zeitdauer",
  age: "Alter",
  jobTitle: "Berufsbezeichnung",
  deviceType: "Endgerät",
  carName: "Auto"
};

const numberFields = ["age"];

const LetterSchema = Yup.object().shape({
  ...Object.keys(fields).reduce(
    (acc, fieldName) => ({
      ...acc,
      [fieldName]: Yup.string()
        .min(2, "Das ist zu kurz!")
        .max(50, "Das ist etwas zu lang!")
        .required("Füll' das doch bitte noch aus.")
    }),
    {}
  ),
  ...numberFields.reduce(
    (acc, numberFieldName) => ({
      ...acc,
      [numberFieldName]: Yup.number()
        .min(0, "Wie soll das denn gehen?")
        .max(130, "Als ob.. Das ist zu alt!")
        .required("Füll' das doch bitte noch aus.")
    }),
    {}
  )
});

const initialValues = Object.keys(fields).reduce(
  (acc, fieldName) => ({ ...acc, [fieldName]: "" }),
  {}
);

const StyledForm = styled(Form)`
  padding: 1rem 1.5rem 3rem;
`;
const FormItem = styled("div")``;

const StyledApp = styled("div")`
  max-width: 60rem;
  margin: 0 auto 3rem;
`;

const StyledLabel = styled("label")`
  display: block;
  font-size: 1.75rem;
  margin: 2rem 0 0.5rem;
`;

const StyledField = styled(Field)`
  display: block;
  font-size: 2rem;
  border: none;
  -webkit-appearance: none;
  padding: 0.3rem 0.5rem;
  background-color: #f6f6f6;
  width: 100%;
`;

const Button = styled("button")`
  -webkit-appearance: none;
  cursor: pointer;
  background-color: transparent;
  border: 1px solid #fff;
  border-radius: 6px;
  outline: none;
  font-size: 1.5rem;
  padding: 0.6rem 1rem;
  margin: 0.5rem 0.75rem;

  color: #fff;
  transition-property: background-color, border-color;
  transition-duration: 300ms;
  transition-timing-function: ease;

  &:hover {
    background-color: #333;
    border-color: #333;
  }

  &:active {
    background-color: #111;
    border-color: #111;
  }

  &[type="submit"],
  &.primary {
    background-color: #ffb86e;
    border-color: #ffb86e;
    &:hover {
      background-color: #f98a15;
      border-color: #f98a15;
    }

    &:active {
      background-color: #f00;
      border-color: #f00;
    }
  }

  @media print {
    display: none;
  }
`;

const StyledErrorMessage = styled(ErrorMessage)`
  color: red;
  border: 1px solid red;
  border-radius: 6px;
  padding: 0.5rem 1rem;
`;

const Logo = styled("img")`
  filter: brightness(10);
  height: 3rem;
  margin: 1rem;
`;

const ButtonBar = styled("div")`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  margin: 2rem -0.5rem 0;
`;

const Heading = styled("h1")`
  text-align: center;
  font-size: 1.7rem;
  font-weight: bold;
  margin: 0;
`;

const Header = styled("header")`
  display: flex;
  align-items: center;
  justify-content: space-between;
  > * {
    flex: 1 0 10rem;
  }
`;

function useHistoryForGapFields(initialValues) {
  const [gapFields, setGapFields] = useState(initialValues);

  function getHistoryFromLocalStorage() {
    try {
      return JSON.parse(localStorage.getItem("history")) || [];
    } catch (e) {
      console.error(e);
      return [];
    }
  }

  const [history, setHistory] = useState(getHistoryFromLocalStorage());

  useEffect(
    () => {
      localStorage.setItem("history", JSON.stringify(history));
    },
    [history]
  );

  return {
    gapFields,
    history,
    setHistory,
    setGapFields: fields => {
      setGapFields(fields);
      if (history.indexOf(fields) === -1) {
        setHistory([...history, fields]);
      }
    }
  };
}

const StyledHistoryTiles = styled("div")`
  padding: 1rem 1.5rem 3rem;

  @media print {
    display: none;
  }
`;

const HistoryTiles = ({ history, setFields, setHistory }) => {
  return (
    <StyledHistoryTiles>
      <h2>History</h2>
      <ButtonBar style={{ justifyContent: "flex-start" }}>
        {history.map((fields, index) => (
          <Button
            className="primary"
            key={index}
            onClick={() => setFields(fields)}
          >
            {fields.promName}
          </Button>
        ))}
        <Button onClick={() => setHistory([])}>&times; clear</Button>
      </ButtonBar>
    </StyledHistoryTiles>
  );
};

const App = () => {
  const {
    gapFields,
    setHistory,
    setGapFields,
    history
  } = useHistoryForGapFields(initialValues);
  const [submitted, setSubmitted] = useState(false);

  return (
    <>
      <Snow />

      <Header>
        <div>
          <Logo
            src="https://www.incloud.de/images/logo.png"
            alt="Incloud Logo"
          />
        </div>
        <Heading>Füll' den Lückentext</Heading>
        <div />
      </Header>

      <StyledApp>
        {!submitted ? (
          <Formik
            initialValues={initialValues}
            validationSchema={LetterSchema}
            onSubmit={v => {
              setGapFields(v);
              setSubmitted(true);
            }}
          >
            {() => (
              <StyledForm>
                {Object.keys(fields).map(fieldName => (
                  <FormItem key={fieldName}>
                    <StyledLabel htmlFor={fieldName}>
                      {fields[fieldName]}
                    </StyledLabel>
                    <StyledField
                      name={fieldName}
                      type={
                        numberFields.indexOf(fieldName) !== -1
                          ? "number"
                          : "text"
                      }
                    />
                    <StyledErrorMessage name={fieldName} component="p" />
                  </FormItem>
                ))}
                <ButtonBar>
                  <Button type="submit">Senden</Button>
                  <Button type="reset">Zurücksetzen</Button>
                </ButtonBar>
              </StyledForm>
            )}
          </Formik>
        ) : (
          <>
            <Letter {...gapFields} />
            <Button
              onClick={() => setSubmitted(false)}
              css={css`
                margin-left: 0.5rem;
                margin-bottom: 2rem;
              `}
            >
              Nochmal von vorne
            </Button>
          </>
        )}
        {history && history.length > 1 && (
          <>
            <hr />
            <HistoryTiles
              history={history}
              setFields={fields => {
                setGapFields(fields);
                setSubmitted(true);
              }}
              setHistory={setHistory}
            />
          </>
        )}
      </StyledApp>
    </>
  );
};

const StyledLetter = styled("div")`
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
  background: #ffffff;
  color: #333;
  padding: 1rem 1.5rem 3rem;
  margin: 3rem 0.5rem;
  font-family: "Indie Flower", sans-serif;
  font-size: 1.35rem;
  word-break: break-all;
  word-break: break-word;

  @media print {
    box-shadow: none;
  }
`;

const StyledB = styled("b")`
  font-size: 3rem;
`;

const Letter = ({
  promName = "",
  funnyAnimal = "",
  tld = "",
  employee = "",
  funnyWord = "",
  language = "",
  petName = "",
  badWord = "",
  goodWord = "",
  subjectPlural = "",
  verb = "",
  timeInFuture = "",
  subject = "",
  timeFrame = "",
  age = "",
  jobTitle = "",
  deviceType = "",
  carName = ""
}) => (
  <StyledLetter>
    <h2>
      Projektanfrage von {promName.replace(" ", ".").toLowerCase()}@
      {funnyAnimal.replace(" ", "").toLowerCase()}.
      {tld.replace(".", "").toLowerCase()}
    </h2>
    <h3>Sehr geehrte/r {employee},</h3>
    <p>
      Wir haben eine Anwendung, die im Frontend mit {funnyWord}.js und im
      Backend mit {language} implementiert wurde. Unser bisheriger
      Dienstleister, die Firma {petName}, hat {badWord} gearbeitet, deshalb
      wenden wir uns an Sie, denn unser Eindruck von Ihnen ist durchweg sehr{" "}
      {goodWord}!
    </p>
    <p>
      Die Anwendung ist dafür da, um {subjectPlural} zu {verb}. Der Start des
      Projektes soll {timeInFuture} sein und wir benötigen zur {subject}-Messe
      eine fertige Version. Die Entwicklungsdauer sollte also {timeFrame}{" "}
      betragen.
    </p>
    <p>
      Unsere Endanwender sind {age} Jahre alt, arbeiten als {jobTitle} und
      benutzen alle {deviceType}s.
    </p>
    <p>Das Ganze sollte nicht mehr kosten als ein {carName}.</p>
    <p>
      Beste Grüße<StyledB>,</StyledB>
    </p>
    <p>{promName}</p>
  </StyledLetter>
);

export default App;
